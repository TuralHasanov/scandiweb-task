<?php

use Models\Product;
use Models\ProductType;
use Models\Attribute;
use Models\ProductAttribute;
use core\Database;

class Api extends Controller{


    public function products(){


        $products = new Product();
        $prodAttr = new ProductAttribute();
        $productTypes = new ProductType();
        
        $products = $products->orderBy('-id')->findAll();
        $productTypes = $productTypes->findAll();

        $ids = array_column($products,'id');

        $prodAttr = $prodAttr->findAll(['product_id'=>['in',$ids]]);

        $results = [];
        foreach ($products as $product) {
        
            $product['attributes'] = array_values(array_filter($prodAttr, function($attr) use ($product){
                return $attr['product_id'] == $product['id'];
            }));

            $product['product_type'] = current(array_filter($productTypes, function($type) use ($product){
                return $type['id'] == $product['product_type'];
            }));
            
            $results[]=$product;

        }

        header_remove();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode( $results );
    }

    public function product_types(){

        $productTypes = new ProductType();
        $productTypes = $productTypes->findAll();

        header_remove();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode( $productTypes );
    }

    public function product_type($id){

        $attrs = new Attribute();
        $attrs = $attrs->findAll(['product_type'=>['=',$id]]);

        header_remove();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode( $attrs );
    }

    public function products_delete(){

        if($_SERVER['REQUEST_METHOD']==='POST' && $_SERVER["CONTENT_TYPE"]==="application/json"){

            $content = trim(file_get_contents("php://input"));
            $data = json_decode($content, true);

            foreach ($data as $key => $id) {
                $product = new Product();
                $product->setId($id);
                $product->delete();
            }
    
            header_remove();
    
            header('Content-type: application/json; charset=utf-8');
            echo json_encode( $data );
        }else{
            return;
        }
    }
}