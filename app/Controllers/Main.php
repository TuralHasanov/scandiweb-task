<?php

use Models\Product;
use Models\ProductType;
use Models\ProductAttribute;

class Main extends Controller{

    public function index(){

        $this->view('home/index');
    }

    public function addproduct(){
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            $data = $_POST;
            
            $product = new Product();
            $product->setSku(strtoupper($data['sku']));
            $product->setName($data['name']);
            $product->setPrice($data['price']);
            $product->setProductType($data['product_type']);

            try {
                $product->create();

                $product = $product->find(['sku'=>['=',$data['sku']]]);

                $prodAttr = new ProductAttribute();

                foreach ($data['attributes'] as $attr => $value) {
                    $prodAttr->setProductId($product['id']);
                    $prodAttr->setAttributeId($attr);
                    $prodAttr->setValue($value);

                    $prodAttr->create();
                }

            } catch (\Exception $e) {
                die($e->getMessage());
            }
            
            redirect('');

        } else {

            $this->view('home/add_product');
        }
        
    }

}