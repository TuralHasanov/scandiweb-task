<?php

namespace Models;

use Models\Model;

class ProductType extends Model{
    
    protected $table = 'product_types';
    protected $db_fields = ['id', 'name'];
    protected $primary_key = 'id';
    protected $class = 'ProductType';

    private $name;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }
    
    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }
}