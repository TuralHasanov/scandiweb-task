<?php

namespace Models;

use Models\Model;

class Attribute extends Model{
    
    protected $table = 'attributes';
    protected $db_fields = ['id', 'name', 'unit'];
    protected $primary_keys = 'id';
    protected $class = 'Attribute';

    private $name;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }
    
    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getUnit(){
        return $this->unit;
    }

    public function setUnit($unit){
        $this->unit = $unit;
    }

}