<?php

namespace Models;

use Models\Model;

class ProductAttribute extends Model{
    
    protected $table = 'product_attribute';
    protected $db_fields = ['id', 'product_id', 'attribute_id', 'value'];
    protected $primary_key = 'id';
    protected $class = 'ProductAttribute';

    private $id;
    private $product_id;
    private $attribute_id;
    private $value;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }
    
    public function getProductId(){
        return $this->product_id;
    }

    public function setProductId($product_id){
        $this->product_id = $product_id;
    }

    public function getAttributeId(){
        return $this->attribute_id;
    }

    public function setAttributeId($attribute_id){
        $this->attribute_id = $attribute_id;
    }

    public function getValue(){
        return $this->value;
    }

    public function setValue($value){
        $this->value = $value;
    }
}