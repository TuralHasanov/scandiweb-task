<?php

namespace Models;

use core\Database;

class Model{

    private $db;

    public function __construct(){
       $this->db = new Database();
    }


    public function create(){
        foreach ($this->db_fields as $key) {
            $data[$key] = $this->$key;
        }

        $this->db->insert($this->table, $data);
    }

  
    public function delete(){
        $key = $this->primary_key;
        $where = $this->primary_key."=".$this->$key;
        $this->db->delete($this->table, $where);
    }


    public function select($cols='*'){
        $this->db->setField('columns', $cols);

        return $this;
    }

    public function orderBy($order = '', $limit = null, $offset = ''){
       
        $this->db->setField('order', $order);
        $this->db->setField('limit', $limit);
        $this->db->setField('offset', $offset);

        return $this;
    }

    public function find($conditions = [])
    {
        try{
            $result = $this->db->find($this, $conditions);
            return $result;
        }catch (Exception $e){
            throw $e;
        }
    }


    public function findAll($conditions = []){
        try{
            $result = $this->db->findAll($this, $conditions);
            
            return $result;
        }catch (Exception $e){
            throw $e;
        }
    }
}