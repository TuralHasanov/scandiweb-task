<?php

namespace Models;

use Models\Model;

class Product extends Model{
    
    protected $table = 'products';
    protected $db_fields = ['id', 'name', 'sku', 'price', 'product_type'];
    protected $primary_key = 'id';
    protected $class = 'Product';

    private $id;
    private $name;
    private $sku;
    private $price;
    private $product_type;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }
    
    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getSku(){
        return $this->sku;
    }

    public function setSku($sku){
        $this->sku = $sku;
    }

    public function getPrice(){
        return $this->price;
    }

    public function setPrice($price){
        $this->price = $price;
    }

    public function getProductType(){
        return $this->product_type;
    }

    public function setProductType($product_type){
        $this->product_type = $product_type;
    }

}