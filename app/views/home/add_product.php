<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="js/vue.js"></script>
    <link rel="stylesheet" href="css/style.css">

</head>
<body class="container py-4">

<div id='addProductApp'>

<form method="POST" id="product_form" class="needs-validation">
    <div class="d-flex justify-content-between align-items-center">
        <h1>Product Add</h1>
        <div>
           
            <button type="submit" class="mx-2 btn btn-outline-success">Save</button>
            <a href="/" class="mx-2 btn btn-outline-danger">Cancel</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-md-8">
        
                <div class="row mb-3">
                    <label for="sku" class="col-sm-4 col-form-label">SKU</label>
                    <div class="col-sm-8">
                        <input name="sku" class="form-control" id="sku" required>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="name" class="col-sm-4 col-form-label">Name</label>
                    <div class="col-sm-8">
                        <input name="name" class="form-control" id="name" required>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="price" class="col-sm-4 col-form-label">Price</label>
                    <div class="col-sm-8">
                        <input type="number" step="0.01" name="price" class="form-control" id="price" required>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="productType" class="col-sm-4 col-form-label">Type Switcher</label>
                    <div class="col-sm-8">
                        <select name="product_type" class="form-select" id="productType" v-model="productType" required>
                            <option selected value="">Type Switcher</option>
                            <option :id="type.name" v-for="(type, index) in productTypes" :key="type.id" :value="type.id">
                                {{type.name}}
                            </option>
                        </select>
                    </div>
                </div>
                <div v-if="selectedType" class="row mb-2" v-for="attr in productAttrs" :key="attr.id">
                    <label :for="attr.name" class="col-sm-4 col-form-label"><span class="text-capitalize">{{attr.name}}</span> (<span class="text-uppercase">{{selectedType.unit}}</span>)</label>
                    <div class="col-sm-8">
                        <input step="0.01" type="number" :name="`attributes[${attr.id}]`" class="form-control" :id="attr.name" required>
                        <div :id="attr.name+'Help'" class="form-text">Please, provide <span class="text-capitalize">{{attr.name}}</span></div>
                    </div>
                </div>
            
        </div>
    </div>
</form>
</div>

<script src="js/main.js"></script>

<script>
    var addProductApp = new Vue({
        el: '#addProductApp',
        data(){
            return {
                productTypes:[],
                productType:null,
                productAttrs:[],
                selectedType:{},
            }
        },

        mounted(){
            this.getProductTypes();
        },

        methods:{
            getProductTypes(){
                fetchData('/api/product_types')
                    .then((data)=>{
                        this.productTypes = data
                    })
                    .catch((err)=>{
                        console.log(err)
                    })
            },
        },

        watch:{
            productType(type){
                this.selectedType = this.productTypes.find(el => el.id==type)
                
                if(type!=''){
                    fetchData(`/api/product_type/${type}`)
                    .then((data)=>{
                        this.productAttrs = data
                    })
                    .catch((err)=>{
                        console.log(err)
                    })
                }else{
                    return;
                }
            }
        }
    });
</script>

</body>
</html>