<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="js/vue.js"></script>
    <link rel="stylesheet" href="css/style.css">

</head>
<body class="container py-4">

<div id="productsApp" >

    <div class="d-flex justify-content-between align-items-center">
        <h1>Product List</h1>
        <div>
            <a href="/addproduct" class="mx-2 btn btn-outline-success">ADD</a>
            <button @click="deleteProducts" id="delete-product-btn" class="mx-2 btn btn-outline-danger">MASS DELETE</button>
        </div>
    </div>  

    <hr>

    <main  class="py-4 container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">
            <div v-for="(product, index) in products" :key="product.id" class="product col mb-2 p-2">
                <div class="card">
                    <input class="delete-checkbox form-check-input" @change="toggleProduct(product, $event)" type="checkbox" value="">
                    <div class="card-body text-center">
                        <h5 class="card-title">{{product.sku}}</h5>
                        <p class="card-text">{{product.name}}</p>
                        <p class="card-text">${{product.price}}</p>
                        <p class="card-text">{{product.product_type.measurement}}: {{product.attributes|productAttributesFilter}} {{product.product_type.unit}}</p>
                    </div>
                </div>
            </div>
        </div>
    </main>

</div>

<script src="js/main.js"></script>

<script>
    var productsApp = new Vue({
        el: '#productsApp',
        data(){
            return {
                products:[],
                productTypes:[],
                selectedProducts:[],
            }
        },

        mounted(){
            this.getProducts();
            this.getProductTypes();
        },

        methods:{
            getProducts(){
                fetchData('/api/products')
                    .then((data)=>{
                        this.products = data
                    })
                    .catch((err)=>{
                        console.log(err)
                    })
            },

            getProductTypes(){
                fetchData('/api/product_types')
                    .then((data)=>{
                        this.productTypes = data
                    })
                    .catch((err)=>{
                        console.log(err)
                    })
            },

            toggleProduct(product, event){
                if(event.currentTarget.checked){
                    this.selectedProducts.push(product.id)
                }else{
                    this.selectedProducts = this.selectedProducts.filter((prod)=>{
                        return prod!= product.id
                    })
                }

                
            },

            deleteProducts(){

                fetchData('/api/products_delete', 'POST', this.selectedProducts)
                    .then((data)=>{
                        this.products = this.products.filter((prod)=>{
                            return !this.selectedProducts.includes(prod.id)
                        })
                    })
                    .catch((err)=>{
                        console.log(err)
                    })
            }
        },

        filters: {
            productAttributesFilter: function (attrs) {
                attrs = attrs.map(e => e['value']);
                return attrs.join('x'); 
            }
        }
    });
</script>

</body>
</html>


