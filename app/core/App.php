<?php

class App{

    protected $controller = 'Main';
    protected $method = 'index';
    protected $params = [];

    public function __construct(){
        $url = $this->parseUrl();

        if(!empty($url[0]) && file_exists('../app/Controllers/'.ucfirst($url[0]).'.php')){
            $this->controller = ucfirst($url[0]);
            array_shift($url);
        }

        require_once '../app/Controllers/'.$this->controller.'.php';

        $this->controller = new $this->controller;

        if(!empty($url)){
            if(method_exists($this->controller, $url[0])){
                $this->method = $url[0];
                array_shift($url);
            }
        }

        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->controller, $this->method], $this->params);

    }

    public function parseUrl(){
        if(isset($_GET['url'])){
            return $url = explode('/',filter_var(rtrim($_GET['url'],'/'), FILTER_SANITIZE_URL));
        }
    }
}