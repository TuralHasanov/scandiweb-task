<?php

namespace core;

use Models\Model;

class Database{
    private $host=DB_HOST;
    private $user=DB_USER;
    private $pwd=DB_PWD;
    private $dbname=DB_NAME;

    private $db;
    private $stmt;
    private $error;

    private $fields=[
        'table'=>'',
        'columns'=>'*',
        'order'=>'',
        'limit'=>null,
        'offset'=>'',
    ];

    private $query='';

    public function __construct(){
        $dsn='mysql:host='.$this->host.';dbname='.$this->dbname.';charset=utf8';
        $options=[
            \PDO::ATTR_PERSISTENT=>true,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE=>\PDO::ERRMODE_EXCEPTION
        ];

        try{
            $this->db=new \PDO($dsn,$this->user,$this->pwd,$options);
        }catch(PDOException $e){
            $this->error=$e->getMessage();
            echo $this->error;
        }
    }

    public function prepare($sql){
        $this->stmt=$this->db->prepare($sql);
    }


    public function bind($param,$value,$type=NULL){
         if(is_null($type)){
             switch(true){
                case is_int($value):
                    $type=\PDO::PARAM_INT;
                break;
                case is_bool($value):
                    $type=\PDO::PARAM_BOOL;
                break;
                case is_null($value):
                    $type=\PDO::PARAM_NULL;
                break;
                default:
                    $type=\PDO::PARAM_STR;
                break;
             }
         }
        $this->stmt->bindValue($param,$value,$type);
         
    }

    public function execute(){
        return $this->stmt->execute();
    }

    public function resultSet(){    

        $this->prepare($this->query);
        $this->execute();
        return $this->stmt->fetchAll();
    }

    public function single(){

        $this->prepare($this->query);
        $this->execute();
        return $this->stmt->fetch();
    }


    public function setField($field, $val){
        $this->fields[$field]=$val;
    }

    public function setQuery($query){
        $this->query=$query;
    }

    public function select($table, $fields = '*')
    {
        $fields = is_array($fields) ? implode(', ', $fields) : $fields;
        $query = "SELECT $fields FROM $table ";

        $this->query .= $query;
        
        return $this;
    }

    public function where($conditions = [])
    {
        
        $where = '';
        
        foreach ($conditions as $key => $value) {
            switch ($value[0]) {
                case '=':
                    if(is_string($value[0])){
                        $where .= " ".$key.' = "'.$value[1].'"'." &&";
                    }else{
                        $where .= " ".$key.' = '.$value[1]. " &&";
                    }
                    break;
                case '>':
                    $where .= " ".$key.' > '.$value[1]. " &&";
                    break;
                case '<':
                    $where .= " ".$key.' < '.$value[1]. " &&";
                    break;
                case 'in':
                    $where .= " ".$key.' IN('.implode(', ',$value[1]). ") &&";
                    break;
                default:
                    break;
            }
        }

        
        $where = rtrim($where, '&');

        $this->query .= (!empty($where)) ? 'WHERE'.$where : '';

        return $this;
    }

    public function orderBy($order, $limit, $offset){
        $this->query .= (($limit) ? ' LIMIT ' . $limit : '')
            . (($offset && $limit) ? ' OFFSET ' . $offset : '')
            . (($order) ? ' ORDER BY ' . ltrim($order,'-') : '')
            . (($order && substr($order,0,1)==='-') ? ' DESC ' : '');

        return $this;
    }


    public function insert($table, $data)
    {

        ksort($data);
        $fieldNames = implode(',', array_keys($data));
        $fieldValues = ':'.implode(', :', array_keys($data));

        $query = "INSERT INTO $table ($fieldNames) VALUES ($fieldValues)";
        $this->prepare($query);

        foreach ($data as $k => $v) {
            $this->bind(":$k", $v);
        }

        try {
            $this->execute();
        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function delete($table, $where = '', $limit=1)
    {
        $this->prepare("DELETE FROM $table WHERE $where LIMIT $limit");
        
        try {
            $this->execute();
        } catch (\Exception $e) {
            throw $e;
        }
    }


   
    public function find(Model $model, $conditions = []){
    
        try{

            $this->select($model->table, $this->fields['columns'])
                 ->orderBy($this->fields['order'], $this->fields['limit'], $this->fields['offset'])
                 ->where($conditions);
                 
            return $this->single();

        }catch (\Exception $e){
            throw $e;
        }
    }

    public function findAll(Model $model, $conditions = []){
       
        try{
            
            $this->select($model->table, $this->fields['columns'])
                 ->orderBy($this->fields['order'], $this->fields['limit'], $this->fields['offset'])
                 ->where($conditions);
        
            return $this->resultSet();
        }catch (\Exception $e){
            throw $e;
        }
    }

}