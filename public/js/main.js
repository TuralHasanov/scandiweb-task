var forms = document.querySelectorAll('.needs-validation')


Array.prototype.slice.call(forms)
  .forEach(function (form) {
    form.addEventListener('submit', function (event) {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })


  async function fetchData(url = '', method='GET', data={}) {

    if (method==='POST'){
        const response = await fetch(url, {
            method: method,
            headers: {
            'Content-Type': 'application/json',
            },
            credentials: "same-origin",
            body: JSON.stringify(data),
        });
        
        if(!response.ok) {
        
        return response.json().then(error => { 
            throw new Error(JSON.stringify(error)) 
        })
        }

        return response.json()

    }else{
        const response = await fetch(url, {
            method: method,
            headers: {
            'Content-Type': 'application/json',
            },
            credentials: "same-origin",
        });

        if(!response.ok) {
        return response.json().then(error => { 
            throw new Error(JSON.stringify(error)) 
        })
        }

        return response.json()
    }

}



